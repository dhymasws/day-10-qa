/*
   Licensed to the Apache Software Foundation (ASF) under one or more
   contributor license agreements.  See the NOTICE file distributed with
   this work for additional information regarding copyright ownership.
   The ASF licenses this file to You under the Apache License, Version 2.0
   (the "License"); you may not use this file except in compliance with
   the License.  You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/
var showControllersOnly = false;
var seriesFilter = "";
var filtersOnlySampleSeries = true;

/*
 * Add header in statistics table to group metrics by category
 * format
 *
 */
function summaryTableHeader(header) {
    var newRow = header.insertRow(-1);
    newRow.className = "tablesorter-no-sort";
    var cell = document.createElement('th');
    cell.setAttribute("data-sorter", false);
    cell.colSpan = 1;
    cell.innerHTML = "Requests";
    newRow.appendChild(cell);

    cell = document.createElement('th');
    cell.setAttribute("data-sorter", false);
    cell.colSpan = 3;
    cell.innerHTML = "Executions";
    newRow.appendChild(cell);

    cell = document.createElement('th');
    cell.setAttribute("data-sorter", false);
    cell.colSpan = 7;
    cell.innerHTML = "Response Times (ms)";
    newRow.appendChild(cell);

    cell = document.createElement('th');
    cell.setAttribute("data-sorter", false);
    cell.colSpan = 1;
    cell.innerHTML = "Throughput";
    newRow.appendChild(cell);

    cell = document.createElement('th');
    cell.setAttribute("data-sorter", false);
    cell.colSpan = 2;
    cell.innerHTML = "Network (KB/sec)";
    newRow.appendChild(cell);
}

/*
 * Populates the table identified by id parameter with the specified data and
 * format
 *
 */
function createTable(table, info, formatter, defaultSorts, seriesIndex, headerCreator) {
    var tableRef = table[0];

    // Create header and populate it with data.titles array
    var header = tableRef.createTHead();

    // Call callback is available
    if(headerCreator) {
        headerCreator(header);
    }

    var newRow = header.insertRow(-1);
    for (var index = 0; index < info.titles.length; index++) {
        var cell = document.createElement('th');
        cell.innerHTML = info.titles[index];
        newRow.appendChild(cell);
    }

    var tBody;

    // Create overall body if defined
    if(info.overall){
        tBody = document.createElement('tbody');
        tBody.className = "tablesorter-no-sort";
        tableRef.appendChild(tBody);
        var newRow = tBody.insertRow(-1);
        var data = info.overall.data;
        for(var index=0;index < data.length; index++){
            var cell = newRow.insertCell(-1);
            cell.innerHTML = formatter ? formatter(index, data[index]): data[index];
        }
    }

    // Create regular body
    tBody = document.createElement('tbody');
    tableRef.appendChild(tBody);

    var regexp;
    if(seriesFilter) {
        regexp = new RegExp(seriesFilter, 'i');
    }
    // Populate body with data.items array
    for(var index=0; index < info.items.length; index++){
        var item = info.items[index];
        if((!regexp || filtersOnlySampleSeries && !info.supportsControllersDiscrimination || regexp.test(item.data[seriesIndex]))
                &&
                (!showControllersOnly || !info.supportsControllersDiscrimination || item.isController)){
            if(item.data.length > 0) {
                var newRow = tBody.insertRow(-1);
                for(var col=0; col < item.data.length; col++){
                    var cell = newRow.insertCell(-1);
                    cell.innerHTML = formatter ? formatter(col, item.data[col]) : item.data[col];
                }
            }
        }
    }

    // Add support of columns sort
    table.tablesorter({sortList : defaultSorts});
}

$(document).ready(function() {

    // Customize table sorter default options
    $.extend( $.tablesorter.defaults, {
        theme: 'blue',
        cssInfoBlock: "tablesorter-no-sort",
        widthFixed: true,
        widgets: ['zebra']
    });

    var data = {"OkPercent": 98.95833333333333, "KoPercent": 1.0416666666666667};
    var dataset = [
        {
            "label" : "FAIL",
            "data" : data.KoPercent,
            "color" : "#FF6347"
        },
        {
            "label" : "PASS",
            "data" : data.OkPercent,
            "color" : "#9ACD32"
        }];
    $.plot($("#flot-requests-summary"), dataset, {
        series : {
            pie : {
                show : true,
                radius : 1,
                label : {
                    show : true,
                    radius : 3 / 4,
                    formatter : function(label, series) {
                        return '<div style="font-size:8pt;text-align:center;padding:2px;color:white;">'
                            + label
                            + '<br/>'
                            + Math.round10(series.percent, -2)
                            + '%</div>';
                    },
                    background : {
                        opacity : 0.5,
                        color : '#000'
                    }
                }
            }
        },
        legend : {
            show : true
        }
    });

    // Creates APDEX table
    createTable($("#apdexTable"), {"supportsControllersDiscrimination": true, "overall": {"data": [0.9330357142857143, 500, 1500, "Total"], "isController": false}, "titles": ["Apdex", "T (Toleration threshold)", "F (Frustration threshold)", "Label"], "items": [{"data": [0.7291666666666666, 500, 1500, ""], "isController": true}, {"data": [0.8333333333333334, 500, 1500, "/demo-cafe/login-56"], "isController": false}, {"data": [1.0, 500, 1500, "/demo-app/master/menu/load_list_data?_dc=1676024756389-152"], "isController": false}, {"data": [0.5, 500, 1500, "logout"], "isController": false}, {"data": [1.0, 500, 1500, "/demo-app/apps/themes/default/assets/fonts/SourceSansPro-Regular.woff2-93"], "isController": false}, {"data": [1.0, 500, 1500, "/v1/session/start-33"], "isController": false}, {"data": [1.0, 500, 1500, "/demo-app/login-35"], "isController": false}, {"data": [1.0, 500, 1500, "/demo-app/apps/themes/default/assets/fonts/OpenSans.woff2-8"], "isController": false}, {"data": [1.0, 500, 1500, "/demo-app-32"], "isController": false}, {"data": [1.0, 500, 1500, "/demo-cafe/login-127"], "isController": false}, {"data": [1.0, 500, 1500, "/demo-cafe/login-126"], "isController": false}, {"data": [1.0, 500, 1500, "/demo-app/master/menu-93"], "isController": false}, {"data": [1.0, 500, 1500, "/demo-app/master/menu-139"], "isController": false}, {"data": [1.0, 500, 1500, "/demo-app/logout-97-0"], "isController": false}, {"data": [1.0, 500, 1500, "/demo-app/logout-97-1"], "isController": false}, {"data": [1.0, 500, 1500, "/demo-app/billing/billing_detail-89"], "isController": false}, {"data": [1.0, 500, 1500, "/demo-cafe/logout-100-0"], "isController": false}, {"data": [1.0, 500, 1500, "/demo-cafe/logout-100-1"], "isController": false}, {"data": [1.0, 500, 1500, "/demo-app/apps/themes/default/assets/fonts/SourceSansPro-Regular.woff2-25"], "isController": false}, {"data": [1.0, 500, 1500, "/demo-app/apps/themes/default/assets/fonts/OpenSans.woff2-122"], "isController": false}, {"data": [0.8333333333333334, 500, 1500, "logout-0"], "isController": false}, {"data": [1.0, 500, 1500, "/demo-app/apps/themes/default/assets/fonts/OpenSans-Bold.woff2-131"], "isController": false}, {"data": [0.6666666666666666, 500, 1500, "logout-1"], "isController": false}, {"data": [1.0, 500, 1500, "unauthorized_update_profile"], "isController": false}, {"data": [1.0, 500, 1500, "/demo-app/logout-97"], "isController": false}, {"data": [1.0, 500, 1500, "/demo-app/master/menu/load_list_data?_dc=1676025027924-154"], "isController": false}, {"data": [1.0, 500, 1500, "/demo-app/apps/themes/default/assets/fonts/OpenSans-Bold.woff2-79"], "isController": false}, {"data": [1.0, 500, 1500, "/demo-cafe/logout-55-0"], "isController": false}, {"data": [1.0, 500, 1500, "/demo-app/master/menu_detail/loadMenu?_dc=1676262865487-43"], "isController": false}, {"data": [1.0, 500, 1500, "create_food"], "isController": false}, {"data": [1.0, 500, 1500, "/maps/api/mapsjs/gen_204-11"], "isController": false}, {"data": [1.0, 500, 1500, "/demo-cafe/logout-13-1"], "isController": false}, {"data": [1.0, 500, 1500, "/demo-app/apps/themes/default/assets/fonts/OpenSans-Semibold.woff2-53"], "isController": false}, {"data": [1.0, 500, 1500, "/demo-cafe/logout-13-0"], "isController": false}, {"data": [1.0, 500, 1500, "/demo-cafe/logout-55-1"], "isController": false}, {"data": [1.0, 500, 1500, "/demo-app/login-2-0"], "isController": false}, {"data": [1.0, 500, 1500, "/demo-app/login-2-1"], "isController": false}, {"data": [1.0, 500, 1500, "/demo-app/master/menu/load_list_data?_dc=1676262850363-37"], "isController": false}, {"data": [1.0, 500, 1500, "/demo-app/apps/themes/default/assets/fonts/OpenSans-Bold.woff2-83"], "isController": false}, {"data": [1.0, 500, 1500, "/demo-app/login-98"], "isController": false}, {"data": [1.0, 500, 1500, "/demo-app/apps/themes/default/assets/fonts/OpenSans.woff2-51"], "isController": false}, {"data": [1.0, 500, 1500, "/demo-cafe/login-102"], "isController": false}, {"data": [0.0, 500, 1500, "delete_food"], "isController": false}, {"data": [1.0, 500, 1500, "/demo-app/apps/themes/default/assets/fonts/SourceSansPro-Semibold.woff2-123"], "isController": false}, {"data": [1.0, 500, 1500, "/demo-app/backend-132"], "isController": false}, {"data": [1.0, 500, 1500, "success-login"], "isController": false}, {"data": [0.8333333333333334, 500, 1500, "/demo-app/master/menu/load_list_data?_dc=1676025451218-175"], "isController": false}, {"data": [1.0, 500, 1500, "invalid-login"], "isController": false}, {"data": [1.0, 500, 1500, "/demo-app/login-16"], "isController": false}, {"data": [1.0, 500, 1500, "/demo-cafe/logout-55"], "isController": false}, {"data": [1.0, 500, 1500, "/demo-app/backend/change_profile-96"], "isController": false}, {"data": [1.0, 500, 1500, "/demo-cafe/login-96"], "isController": false}, {"data": [1.0, 500, 1500, "/demo-app/master/menu_detail/load_varian?_dc=1676262865485-42"], "isController": false}, {"data": [1.0, 500, 1500, "/demo-cafe/backend/services?_dc=1676024756150-151"], "isController": false}, {"data": [1.0, 500, 1500, "/demo-cafe/login-15"], "isController": false}, {"data": [1.0, 500, 1500, "/demo-app/login-2"], "isController": false}, {"data": [1.0, 500, 1500, "/demo-app/apps/themes/default/assets/fonts/OpenSans-Bold.woff2-18"], "isController": false}, {"data": [1.0, 500, 1500, "/demo-app/apps/themes/default/assets/fonts/SourceSansPro-Semibold.woff2-52"], "isController": false}, {"data": [1.0, 500, 1500, "/demo-app/login/accepted-17"], "isController": false}, {"data": [1.0, 500, 1500, "/j/collect?t=dc&aip=1&_r=3&v=1&_v=j99&tid=UA-77479600-1&cid=1314642526.1676023217&jid=1798033955&gjid=206862942&_gid=376522333.1676023217&_u=AACAAEAAAAAAACAAI~&z=781087054-23"], "isController": false}, {"data": [1.0, 500, 1500, "/demo-app/backend-20"], "isController": false}, {"data": [1.0, 500, 1500, "/demo-app/master/menu_detail-38"], "isController": false}, {"data": [1.0, 500, 1500, "/demo-app/login-77"], "isController": false}, {"data": [1.0, 500, 1500, "/demo-app/-104"], "isController": false}, {"data": [1.0, 500, 1500, "/demo-app/master/menu/load_list_data?_dc=1676269128192-95"], "isController": false}, {"data": [1.0, 500, 1500, "/demo-cafe/logout-121"], "isController": false}, {"data": [1.0, 500, 1500, "/demo-app-32-0"], "isController": false}, {"data": [1.0, 500, 1500, "/demo-app-32-1"], "isController": false}, {"data": [1.0, 500, 1500, "/demo-cafe/logout-121-0"], "isController": false}, {"data": [1.0, 500, 1500, "/demo-app/login-128"], "isController": false}, {"data": [1.0, 500, 1500, "/demo-cafe/logout-72"], "isController": false}, {"data": [1.0, 500, 1500, "/demo-app/apps/themes/default/assets/fonts/OpenSans-Semibold.woff2-124"], "isController": false}, {"data": [1.0, 500, 1500, "/demo-cafe/logout-121-1"], "isController": false}, {"data": [1.0, 500, 1500, "/demo-app/master/menu-26"], "isController": false}, {"data": [1.0, 500, 1500, "/demo-app/login/accepted-78"], "isController": false}, {"data": [1.0, 500, 1500, "/demo-cafe/logout-72-0"], "isController": false}, {"data": [1.0, 500, 1500, "/demo-cafe/logout-72-1"], "isController": false}, {"data": [1.0, 500, 1500, "/demo-app/apps/themes/default/assets/fonts/OpenSans-Semibold.woff2-9"], "isController": false}, {"data": [1.0, 500, 1500, "/demo-app/master/menu/load_list_data?_dc=1676262800543-34"], "isController": false}, {"data": [1.0, 500, 1500, "/demo-app/login/accepted-129"], "isController": false}, {"data": [0.5, 500, 1500, "/demo-app/master/menu/load_list_data?_dc=1676023778288-147"], "isController": false}, {"data": [1.0, 500, 1500, "/demo/app-1"], "isController": false}, {"data": [1.0, 500, 1500, "/v1/session/start-15"], "isController": false}, {"data": [1.0, 500, 1500, "update_food"], "isController": false}, {"data": [0.6666666666666666, 500, 1500, "/v1/session/start-16"], "isController": false}, {"data": [1.0, 500, 1500, "/demo-app/backend-81"], "isController": false}, {"data": [1.0, 500, 1500, "/demo-app/-1"], "isController": false}, {"data": [1.0, 500, 1500, "/demo-app/apps/themes/default/assets/fonts/SourceSansPro-Regular.woff2-135"], "isController": false}, {"data": [1.0, 500, 1500, "/demo-cafe/backend/services?_dc=1676262850061-36"], "isController": false}, {"data": [1.0, 500, 1500, "/demo-cafe/logout-100"], "isController": false}, {"data": [1.0, 500, 1500, "/_s/v4/assets/images/attention-grabbers/168-r-br.svg-31"], "isController": false}, {"data": [1.0, 500, 1500, "/_s/v4/assets/images/attention-grabbers/168-r-br.svg-30"], "isController": false}, {"data": [1.0, 500, 1500, "/demo-app/login-105"], "isController": false}, {"data": [1.0, 500, 1500, "/demo-app/master/menu-155"], "isController": false}, {"data": [1.0, 500, 1500, "read_food"], "isController": false}, {"data": [1.0, 500, 1500, "/demo-cafe/logout-13"], "isController": false}, {"data": [1.0, 500, 1500, "/demo-app/login-59"], "isController": false}]}, function(index, item){
        switch(index){
            case 0:
                item = item.toFixed(3);
                break;
            case 1:
            case 2:
                item = formatDuration(item);
                break;
        }
        return item;
    }, [[0, 0]], 3);

    // Create statistics table
    createTable($("#statisticsTable"), {"supportsControllersDiscrimination": true, "overall": {"data": ["Total", 288, 3, 1.0416666666666667, 166.01041666666669, 22, 1175, 103.5, 356.60000000000014, 477.0000000000002, 878.6300000000045, 20.750774551480657, 205.82622014374235, 14.749126377981122], "isController": false}, "titles": ["Label", "#Samples", "FAIL", "Error %", "Average", "Min", "Max", "Median", "90th pct", "95th pct", "99th pct", "Transactions/s", "Received", "Sent"], "items": [{"data": ["", 48, 0, 0.0, 696.7916666666669, 45, 3301, 462.0, 1337.3000000000015, 2957.4499999999994, 3301.0, 4.182277598675612, 237.2058995708809, 13.338157292846562], "isController": true}, {"data": ["/demo-cafe/login-56", 3, 0, 0.0, 218.33333333333331, 54, 524, 77.0, 524.0, 524.0, 524.0, 3.181336161187699, 9.289253048780488, 1.7646474019088019], "isController": false}, {"data": ["/demo-app/master/menu/load_list_data?_dc=1676024756389-152", 3, 0, 0.0, 349.0, 248, 486, 313.0, 486.0, 486.0, 486.0, 4.504504504504505, 324.99589433183183, 3.400373029279279], "isController": false}, {"data": ["logout", 3, 0, 0.0, 1064.0, 842, 1175, 1175.0, 1175.0, 1175.0, 1175.0, 2.4193548387096775, 8.803238407258064, 3.0123802923387095], "isController": false}, {"data": ["/demo-app/apps/themes/default/assets/fonts/SourceSansPro-Regular.woff2-93", 3, 0, 0.0, 185.33333333333334, 47, 311, 198.0, 311.0, 311.0, 311.0, 2.7598896044158234, 39.94023832796688, 1.827348781048758], "isController": false}, {"data": ["/v1/session/start-33", 3, 0, 0.0, 287.3333333333333, 269, 310, 283.0, 310.0, 310.0, 310.0, 4.026845637583893, 2.2808305369127515, 1.8915163590604027], "isController": false}, {"data": ["/demo-app/login-35", 3, 0, 0.0, 290.0, 67, 449, 354.0, 449.0, 449.0, 449.0, 3.6719706242350063, 15.971637851897185, 2.187404375764994], "isController": false}, {"data": ["/demo-app/apps/themes/default/assets/fonts/OpenSans.woff2-8", 3, 0, 0.0, 56.333333333333336, 39, 73, 57.0, 73.0, 73.0, 73.0, 6.993006993006993, 109.83255026223776, 4.541357080419581], "isController": false}, {"data": ["/demo-app-32", 3, 0, 0.0, 345.6666666666667, 234, 432, 371.0, 432.0, 432.0, 432.0, 3.7267080745341614, 6.2305900621118, 4.494613742236025], "isController": false}, {"data": ["/demo-cafe/login-127", 3, 0, 0.0, 71.33333333333333, 52, 101, 61.0, 101.0, 101.0, 101.0, 5.870841487279844, 5.968306629158513, 4.506329500978474], "isController": false}, {"data": ["/demo-cafe/login-126", 3, 0, 0.0, 110.0, 61, 178, 91.0, 178.0, 178.0, 178.0, 5.44464609800363, 15.897941243194191, 3.2912460299455533], "isController": false}, {"data": ["/demo-app/master/menu-93", 3, 0, 0.0, 90.33333333333333, 51, 127, 93.0, 127.0, 127.0, 127.0, 5.319148936170213, 37.73790724734043, 3.610164561170213], "isController": false}, {"data": ["/demo-app/master/menu-139", 3, 0, 0.0, 62.0, 55, 67, 64.0, 67.0, 67.0, 67.0, 31.578947368421055, 224.0439967105263, 20.970394736842106], "isController": false}, {"data": ["/demo-app/logout-97-0", 3, 0, 0.0, 74.33333333333333, 61, 99, 63.0, 99.0, 99.0, 99.0, 2.7247956403269753, 1.601881811989101, 1.7961299386920981], "isController": false}, {"data": ["/demo-app/logout-97-1", 3, 0, 0.0, 152.66666666666666, 132, 194, 132.0, 194.0, 194.0, 194.0, 2.643171806167401, 11.455465308370044, 1.7397439427312775], "isController": false}, {"data": ["/demo-app/billing/billing_detail-89", 3, 0, 0.0, 55.333333333333336, 51, 63, 52.0, 63.0, 63.0, 63.0, 5.319148936170213, 31.745207225177307, 3.610164561170213], "isController": false}, {"data": ["/demo-cafe/logout-100-0", 3, 0, 0.0, 128.0, 111, 161, 112.0, 161.0, 161.0, 161.0, 2.803738317757009, 2.0398291471962615, 1.5579366238317756], "isController": false}, {"data": ["/demo-cafe/logout-100-1", 3, 0, 0.0, 173.33333333333334, 82, 219, 219.0, 219.0, 219.0, 219.0, 2.6595744680851063, 7.765749667553192, 1.4752327127659577], "isController": false}, {"data": ["/demo-app/apps/themes/default/assets/fonts/SourceSansPro-Regular.woff2-25", 3, 0, 0.0, 47.666666666666664, 26, 75, 42.0, 75.0, 75.0, 75.0, 4.6875, 67.83599853515625, 3.1036376953125], "isController": false}, {"data": ["/demo-app/apps/themes/default/assets/fonts/OpenSans.woff2-122", 3, 0, 0.0, 95.33333333333333, 64, 148, 74.0, 148.0, 148.0, 148.0, 2.952755905511811, 46.37614573080709, 2.0646222933070866], "isController": false}, {"data": ["logout-0", 3, 0, 0.0, 538.0, 426, 762, 426.0, 762.0, 762.0, 762.0, 2.586206896551724, 1.8588362068965518, 1.7401333512931036], "isController": false}, {"data": ["/demo-app/apps/themes/default/assets/fonts/OpenSans-Bold.woff2-131", 3, 0, 0.0, 106.33333333333334, 42, 181, 96.0, 181.0, 181.0, 181.0, 7.978723404255319, 130.79963846409575, 5.220453789893617], "isController": false}, {"data": ["logout-1", 3, 0, 0.0, 525.3333333333334, 80, 748, 748.0, 748.0, 748.0, 748.0, 3.6900369003690034, 10.77461946494465, 2.1116812730627306], "isController": false}, {"data": ["unauthorized_update_profile", 3, 0, 0.0, 118.0, 54, 222, 78.0, 222.0, 222.0, 222.0, 13.513513513513514, 9.726034628378379, 10.293496621621621], "isController": false}, {"data": ["/demo-app/logout-97", 3, 0, 0.0, 227.66666666666666, 194, 293, 196.0, 293.0, 293.0, 293.0, 2.4311183144246353, 11.965660453808752, 3.2027134825769856], "isController": false}, {"data": ["/demo-app/master/menu/load_list_data?_dc=1676025027924-154", 3, 0, 0.0, 402.6666666666667, 356, 466, 386.0, 466.0, 466.0, 466.0, 3.389830508474576, 244.7596663135593, 2.558924788135593], "isController": false}, {"data": ["/demo-app/apps/themes/default/assets/fonts/OpenSans-Bold.woff2-79", 3, 0, 0.0, 126.66666666666666, 49, 254, 77.0, 254.0, 254.0, 254.0, 3.0643513789581203, 50.23561191266599, 2.004995531154239], "isController": false}, {"data": ["/demo-cafe/logout-55-0", 3, 0, 0.0, 66.33333333333333, 63, 71, 65.0, 71.0, 71.0, 71.0, 3.131524008350731, 2.278306041231733, 1.740075352296451], "isController": false}, {"data": ["/demo-app/master/menu_detail/loadMenu?_dc=1676262865487-43", 3, 0, 0.0, 69.33333333333333, 50, 107, 51.0, 107.0, 107.0, 107.0, 5.319148936170213, 5.490566821808511, 4.1400016622340425], "isController": false}, {"data": ["create_food", 3, 0, 0.0, 148.0, 123, 195, 126.0, 195.0, 195.0, 195.0, 5.3097345132743365, 3.90970685840708, 4.858614491150443], "isController": false}, {"data": ["/maps/api/mapsjs/gen_204-11", 3, 0, 0.0, 92.0, 91, 93, 92.0, 93.0, 93.0, 93.0, 20.54794520547945, 13.932737585616438, 8.247270976027398], "isController": false}, {"data": ["/demo-cafe/logout-13-1", 3, 0, 0.0, 55.333333333333336, 47, 69, 50.0, 69.0, 69.0, 69.0, 5.825242718446602, 17.0092536407767, 3.2311893203883493], "isController": false}, {"data": ["/demo-app/apps/themes/default/assets/fonts/OpenSans-Semibold.woff2-53", 3, 0, 0.0, 49.666666666666664, 34, 74, 41.0, 74.0, 74.0, 74.0, 3.236245954692557, 52.69961063915857, 2.1301072006472492], "isController": false}, {"data": ["/demo-cafe/logout-13-0", 3, 0, 0.0, 64.0, 49, 80, 63.0, 80.0, 80.0, 80.0, 5.893909626719057, 4.288049484282908, 3.27503376719057], "isController": false}, {"data": ["/demo-cafe/logout-55-1", 3, 0, 0.0, 67.0, 50, 78, 73.0, 78.0, 78.0, 78.0, 3.1746031746031744, 9.269593253968255, 1.7609126984126986], "isController": false}, {"data": ["/demo-app/login-2-0", 3, 0, 0.0, 58.666666666666664, 46, 72, 58.0, 72.0, 72.0, 72.0, 7.352941176470588, 4.337086397058823, 4.3801700367647065], "isController": false}, {"data": ["/demo-app/login-2-1", 3, 0, 0.0, 66.0, 57, 74, 67.0, 74.0, 74.0, 74.0, 6.976744186046512, 44.858284883720934, 4.169694767441861], "isController": false}, {"data": ["/demo-app/master/menu/load_list_data?_dc=1676262850363-37", 3, 0, 0.0, 315.0, 263, 402, 280.0, 402.0, 402.0, 402.0, 3.745318352059925, 270.0079490480649, 2.8272764513108615], "isController": false}, {"data": ["/demo-app/apps/themes/default/assets/fonts/OpenSans-Bold.woff2-83", 3, 0, 0.0, 135.0, 71, 229, 105.0, 229.0, 229.0, 229.0, 2.997002997002997, 49.13153252997004, 1.9609296953046955], "isController": false}, {"data": ["/demo-app/login-98", 3, 0, 0.0, 221.0, 142, 261, 260.0, 261.0, 261.0, 261.0, 2.7247956403269753, 11.851796662125341, 1.7934690054495914], "isController": false}, {"data": ["/demo-app/apps/themes/default/assets/fonts/OpenSans.woff2-51", 3, 0, 0.0, 83.33333333333333, 81, 85, 84.0, 85.0, 85.0, 85.0, 3.592814371257485, 56.42893899700599, 2.3332241766467066], "isController": false}, {"data": ["/demo-cafe/login-102", 3, 0, 0.0, 75.66666666666667, 75, 77, 75.0, 77.0, 77.0, 77.0, 2.6714158504007126, 7.800325578806768, 1.4818009795191451], "isController": false}, {"data": ["delete_food", 3, 3, 100.0, 285.3333333333333, 204, 415, 237.0, 415.0, 415.0, 415.0, 3.6855036855036856, 3.318392966830467, 2.3322328009828013], "isController": false}, {"data": ["/demo-app/apps/themes/default/assets/fonts/SourceSansPro-Semibold.woff2-123", 3, 0, 0.0, 169.33333333333334, 29, 403, 76.0, 403.0, 403.0, 403.0, 3.1746031746031744, 44.96217757936508, 2.2631448412698414], "isController": false}, {"data": ["/demo-app/backend-132", 3, 0, 0.0, 66.33333333333333, 48, 100, 51.0, 100.0, 100.0, 100.0, 17.647058823529413, 113.46507352941175, 11.615349264705882], "isController": false}, {"data": ["success-login", 3, 0, 0.0, 89.0, 75, 115, 77.0, 115.0, 115.0, 115.0, 5.39568345323741, 5.485260229316546, 3.8623398156474815], "isController": false}, {"data": ["/demo-app/master/menu/load_list_data?_dc=1676025451218-175", 3, 0, 0.0, 486.6666666666667, 408, 601, 451.0, 601.0, 601.0, 601.0, 3.1914893617021276, 230.43862200797875, 2.490234375], "isController": false}, {"data": ["invalid-login", 3, 0, 0.0, 139.66666666666666, 123, 173, 123.0, 173.0, 173.0, 173.0, 5.758157389635317, 4.554792466410748, 4.155545225527831], "isController": false}, {"data": ["/demo-app/login-16", 3, 0, 0.0, 55.666666666666664, 43, 78, 46.0, 78.0, 78.0, 78.0, 5.199306759098787, 5.03682842287695, 3.9857966854419415], "isController": false}, {"data": ["/demo-cafe/logout-55", 3, 0, 0.0, 133.33333333333334, 115, 144, 141.0, 144.0, 144.0, 144.0, 2.976190476190476, 10.855538504464286, 3.3046177455357144], "isController": false}, {"data": ["/demo-app/backend/change_profile-96", 3, 0, 0.0, 52.0, 45, 57, 54.0, 57.0, 57.0, 57.0, 7.792207792207792, 49.54596185064935, 5.2886566558441555], "isController": false}, {"data": ["/demo-cafe/login-96", 3, 0, 0.0, 156.66666666666666, 123, 223, 124.0, 223.0, 223.0, 223.0, 2.819548872180451, 8.232862429511277, 1.6878744713345863], "isController": false}, {"data": ["/demo-app/master/menu_detail/load_varian?_dc=1676262865485-42", 3, 0, 0.0, 54.333333333333336, 47, 59, 57.0, 59.0, 59.0, 59.0, 5.847953216374268, 5.602384868421052, 4.4373629385964914], "isController": false}, {"data": ["/demo-cafe/backend/services?_dc=1676024756150-151", 3, 0, 0.0, 116.66666666666667, 94, 131, 125.0, 131.0, 131.0, 131.0, 9.615384615384617, 7.080078125, 9.296123798076923], "isController": false}, {"data": ["/demo-cafe/login-15", 3, 0, 0.0, 59.0, 46, 83, 48.0, 83.0, 83.0, 83.0, 5.484460694698354, 5.575511311700183, 4.20975205667276], "isController": false}, {"data": ["/demo-app/login-2", 3, 0, 0.0, 125.0, 103, 146, 126.0, 146.0, 146.0, 146.0, 6.302521008403361, 44.24074317226891, 7.5211725315126055], "isController": false}, {"data": ["/demo-app/apps/themes/default/assets/fonts/OpenSans-Bold.woff2-18", 3, 0, 0.0, 38.666666666666664, 22, 47, 47.0, 47.0, 47.0, 47.0, 4.87012987012987, 79.8387403612013, 3.1865107548701297], "isController": false}, {"data": ["/demo-app/apps/themes/default/assets/fonts/SourceSansPro-Semibold.woff2-52", 3, 0, 0.0, 97.33333333333333, 44, 144, 104.0, 144.0, 144.0, 144.0, 3.34075723830735, 47.31543186247216, 2.2152091453229397], "isController": false}, {"data": ["/demo-app/login/accepted-17", 3, 0, 0.0, 65.33333333333333, 45, 82, 69.0, 82.0, 82.0, 82.0, 4.885993485342019, 8.144912968241043, 3.788553542345277], "isController": false}, {"data": ["/j/collect?t=dc&aip=1&_r=3&v=1&_v=j99&tid=UA-77479600-1&cid=1314642526.1676023217&jid=1798033955&gjid=206862942&_gid=376522333.1676023217&_u=AACAAEAAAAAAACAAI~&z=781087054-23", 3, 0, 0.0, 116.33333333333333, 92, 137, 120.0, 137.0, 137.0, 137.0, 6.772009029345372, 3.796028498871332, 3.928294300225734], "isController": false}, {"data": ["/demo-app/backend-20", 3, 0, 0.0, 93.33333333333333, 66, 144, 70.0, 144.0, 144.0, 144.0, 4.518072289156627, 29.049792921686745, 2.973809299698795], "isController": false}, {"data": ["/demo-app/master/menu_detail-38", 3, 0, 0.0, 115.66666666666667, 46, 193, 108.0, 193.0, 193.0, 193.0, 4.63678516228748, 33.74800763137558, 3.165149246522411], "isController": false}, {"data": ["/demo-app/login-77", 3, 0, 0.0, 146.33333333333331, 69, 295, 75.0, 295.0, 295.0, 295.0, 3.7128712871287126, 3.5968440594059405, 2.846292930074257], "isController": false}, {"data": ["/demo-app/-104", 3, 0, 0.0, 179.0, 131, 203, 203.0, 203.0, 203.0, 203.0, 2.398081534772182, 1.384049010791367, 1.5339291067146283], "isController": false}, {"data": ["/demo-app/master/menu/load_list_data?_dc=1676269128192-95", 3, 0, 0.0, 277.0, 199, 329, 303.0, 329.0, 329.0, 329.0, 4.484304932735426, 323.72827914798205, 3.3851247197309413], "isController": false}, {"data": ["/demo-cafe/logout-121", 3, 0, 0.0, 143.0, 93, 233, 103.0, 233.0, 233.0, 233.0, 2.5337837837837838, 9.241877375422298, 3.0657794024493246], "isController": false}, {"data": ["/demo-app-32-0", 3, 0, 0.0, 269.3333333333333, 191, 336, 281.0, 336.0, 336.0, 336.0, 4.2313117066290555, 4.632129319464034, 2.549530588857546], "isController": false}, {"data": ["/demo-app-32-1", 3, 0, 0.0, 76.33333333333333, 43, 96, 90.0, 96.0, 96.0, 96.0, 5.7251908396946565, 3.3042849475190836, 3.4552421278625953], "isController": false}, {"data": ["/demo-cafe/logout-121-0", 3, 0, 0.0, 73.66666666666667, 45, 120, 56.0, 120.0, 120.0, 120.0, 2.638522427440633, 1.9196281332453826, 1.5975428759894459], "isController": false}, {"data": ["/demo-app/login-128", 3, 0, 0.0, 100.33333333333333, 52, 162, 87.0, 162.0, 162.0, 162.0, 6.036217303822937, 5.847585513078471, 4.6273736167002015], "isController": false}, {"data": ["/demo-cafe/logout-72", 3, 0, 0.0, 215.33333333333334, 168, 255, 223.0, 255.0, 255.0, 255.0, 4.405286343612335, 16.068109856828194, 5.115122522026431], "isController": false}, {"data": ["/demo-app/apps/themes/default/assets/fonts/OpenSans-Semibold.woff2-124", 3, 0, 0.0, 199.33333333333334, 96, 276, 226.0, 276.0, 276.0, 276.0, 3.911342894393742, 63.69301051173403, 2.7692613265971318], "isController": false}, {"data": ["/demo-cafe/logout-121-1", 3, 0, 0.0, 68.66666666666667, 47, 112, 47.0, 112.0, 112.0, 112.0, 2.8222013170272815, 8.24060736124177, 1.7059986476952023], "isController": false}, {"data": ["/demo-app/master/menu-26", 3, 0, 0.0, 91.66666666666667, 64, 145, 66.0, 145.0, 145.0, 145.0, 4.424778761061947, 31.355146109882003, 2.938329646017699], "isController": false}, {"data": ["/demo-app/login/accepted-78", 3, 0, 0.0, 136.0, 52, 295, 61.0, 295.0, 295.0, 295.0, 3.821656050955414, 6.370670780254777, 2.9632762738853504], "isController": false}, {"data": ["/demo-cafe/logout-72-0", 3, 0, 0.0, 106.0, 64, 153, 101.0, 153.0, 153.0, 153.0, 4.909983633387888, 3.572204889525368, 2.852969005728314], "isController": false}, {"data": ["/demo-cafe/logout-72-1", 3, 0, 0.0, 109.0, 70, 153, 104.0, 153.0, 153.0, 153.0, 4.862236628849271, 14.197351094003242, 2.820477106969206], "isController": false}, {"data": ["/demo-app/apps/themes/default/assets/fonts/OpenSans-Semibold.woff2-9", 3, 0, 0.0, 72.0, 40, 112, 64.0, 112.0, 112.0, 112.0, 6.3965884861407245, 104.16319629530918, 4.210254530916845], "isController": false}, {"data": ["/demo-app/master/menu/load_list_data?_dc=1676262800543-34", 3, 0, 0.0, 284.3333333333333, 259, 313, 281.0, 313.0, 313.0, 313.0, 3.6855036855036856, 265.91292997543, 2.782123387592138], "isController": false}, {"data": ["/demo-app/login/accepted-129", 3, 0, 0.0, 73.33333333333333, 48, 121, 51.0, 121.0, 121.0, 121.0, 12.345679012345679, 20.580150462962962, 9.5727237654321], "isController": false}, {"data": ["/demo-app/master/menu/load_list_data?_dc=1676023778288-147", 3, 0, 0.0, 606.6666666666666, 525, 673, 622.0, 673.0, 673.0, 673.0, 4.213483146067416, 303.85220988412925, 3.180686007724719], "isController": false}, {"data": ["/demo/app-1", 3, 0, 0.0, 100.0, 54, 169, 77.0, 169.0, 169.0, 169.0, 13.574660633484163, 58.59375, 7.476668552036199], "isController": false}, {"data": ["/v1/session/start-15", 3, 0, 0.0, 329.3333333333333, 321, 336, 331.0, 336.0, 336.0, 336.0, 7.731958762886598, 4.379429768041237, 3.5337467783505154], "isController": false}, {"data": ["update_food", 3, 0, 0.0, 121.66666666666667, 113, 134, 118.0, 134.0, 134.0, 134.0, 5.964214711729622, 4.403267892644135, 5.341000869781312], "isController": false}, {"data": ["/v1/session/start-16", 3, 0, 0.0, 562.3333333333334, 439, 699, 549.0, 699.0, 699.0, 699.0, 4.225352112676056, 5.838743397887324, 2.6037081866197185], "isController": false}, {"data": ["/demo-app/backend-81", 3, 0, 0.0, 102.33333333333333, 71, 121, 115.0, 121.0, 121.0, 121.0, 2.9702970297029703, 19.098081683168317, 1.9550587871287128], "isController": false}, {"data": ["/demo-app/-1", 3, 0, 0.0, 68.66666666666667, 63, 75, 68.0, 75.0, 75.0, 75.0, 7.246376811594203, 46.5919384057971, 4.373301630434783], "isController": false}, {"data": ["/demo-app/apps/themes/default/assets/fonts/SourceSansPro-Regular.woff2-135", 3, 0, 0.0, 106.66666666666667, 55, 153, 112.0, 153.0, 153.0, 153.0, 19.48051948051948, 281.9158380681818, 12.898234577922079], "isController": false}, {"data": ["/demo-cafe/backend/services?_dc=1676262850061-36", 3, 0, 0.0, 108.0, 96, 118, 110.0, 118.0, 118.0, 118.0, 4.854368932038835, 3.5744083737864076, 4.697929308252427], "isController": false}, {"data": ["/demo-cafe/logout-100", 3, 0, 0.0, 302.3333333333333, 244, 332, 331.0, 332.0, 332.0, 332.0, 2.3255813953488373, 8.482467296511627, 2.5822129360465116], "isController": false}, {"data": ["/_s/v4/assets/images/attention-grabbers/168-r-br.svg-31", 3, 0, 0.0, 61.333333333333336, 29, 90, 65.0, 90.0, 90.0, 90.0, 5.474452554744526, 34.00326471259124, 2.084996578467153], "isController": false}, {"data": ["/_s/v4/assets/images/attention-grabbers/168-r-br.svg-30", 3, 0, 0.0, 297.6666666666667, 202, 362, 329.0, 362.0, 362.0, 362.0, 4.37956204379562, 27.20261177007299, 1.787750912408759], "isController": false}, {"data": ["/demo-app/login-105", 3, 0, 0.0, 150.33333333333334, 137, 176, 138.0, 176.0, 176.0, 176.0, 2.3847376788553256, 10.372677364864865, 1.5393668024642289], "isController": false}, {"data": ["/demo-app/master/menu-155", 3, 0, 0.0, 69.33333333333333, 49, 83, 76.0, 83.0, 83.0, 83.0, 5.163511187607574, 36.54629679432014, 3.559998924268503], "isController": false}, {"data": ["read_food", 3, 0, 0.0, 150.66666666666666, 150, 152, 150.0, 152.0, 152.0, 152.0, 5.769230769230769, 4.1522686298076925, 3.7804236778846154], "isController": false}, {"data": ["/demo-cafe/logout-13", 3, 0, 0.0, 119.66666666666667, 97, 149, 113.0, 149.0, 149.0, 149.0, 5.190311418685121, 18.93145815311419, 5.763070393598617], "isController": false}, {"data": ["/demo-app/login-59", 3, 0, 0.0, 76.33333333333333, 62, 102, 65.0, 102.0, 102.0, 102.0, 5.758157389635317, 25.0457353646833, 3.6494571737044144], "isController": false}]}, function(index, item){
        switch(index){
            // Errors pct
            case 3:
                item = item.toFixed(2) + '%';
                break;
            // Mean
            case 4:
            // Mean
            case 7:
            // Median
            case 8:
            // Percentile 1
            case 9:
            // Percentile 2
            case 10:
            // Percentile 3
            case 11:
            // Throughput
            case 12:
            // Kbytes/s
            case 13:
            // Sent Kbytes/s
                item = item.toFixed(2);
                break;
        }
        return item;
    }, [[0, 0]], 0, summaryTableHeader);

    // Create error table
    createTable($("#errorsTable"), {"supportsControllersDiscrimination": false, "titles": ["Type of error", "Number of errors", "% in errors", "% in all samples"], "items": [{"data": ["Test failed: message expected to equal /\\n\\n****** received  : [[[OK ]]]\\n\\n****** comparison: [[[200]]]\\n\\n/", 3, 100.0, 1.0416666666666667], "isController": false}]}, function(index, item){
        switch(index){
            case 2:
            case 3:
                item = item.toFixed(2) + '%';
                break;
        }
        return item;
    }, [[1, 1]]);

        // Create top5 errors by sampler
    createTable($("#top5ErrorsBySamplerTable"), {"supportsControllersDiscrimination": false, "overall": {"data": ["Total", 288, 3, "Test failed: message expected to equal /\\n\\n****** received  : [[[OK ]]]\\n\\n****** comparison: [[[200]]]\\n\\n/", 3, "", "", "", "", "", "", "", ""], "isController": false}, "titles": ["Sample", "#Samples", "#Errors", "Error", "#Errors", "Error", "#Errors", "Error", "#Errors", "Error", "#Errors", "Error", "#Errors"], "items": [{"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": ["delete_food", 3, 3, "Test failed: message expected to equal /\\n\\n****** received  : [[[OK ]]]\\n\\n****** comparison: [[[200]]]\\n\\n/", 3, "", "", "", "", "", "", "", ""], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}]}, function(index, item){
        return item;
    }, [[0, 0]], 0);

});
